<?php

/*
 * Copyright 2020 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Bump;

use Console\Help as Base;

/**
 * Help message util.
 */
class Help extends Base
{
    /**
     * Returns the list of examples for the help message.
     *
     * @return array
     */
    public static function getExamples()
    {
        return [
            'bump -p',
            'bump --minor --files composer.json,package.json',
        ];
    }

    /**
     * Returns the note part of the help message.
     *
     * @return array
     */
    public static function getNotes()
    {
        return [
            'Increment a composer.json file\'s semantic version number. Other files'
            .' can be specified with -f(--files) separated by commas. At least one'
            .' of the bump types must be called(e.g. -m/--major).',
            ' The version is matched by the following regular expressions: '.Bump::REGEX,
        ];
    }

    /**
     * Returns the list of options for the help message.
     *
     * @return array
     */
    public static function getOptions()
    {
        return [
            '-m, --major'       => 'Increments the package\'s MAJOR version number',
            '-n, --minor'       => 'Increments the package\'s MINOR version number',
            '-p, --patch'       => 'Increments the package\'s PATCH version number',
            '-f, --files FILES' => 'Files to version bump',
        ];
    }

    /**
     * Returns the usage part of the help message.
     *
     * @return string
     */
    public static function getUsage()
    {
        return 'Usage: bump [--major|--minor|--patch] [--files FILES]';
    }
}
