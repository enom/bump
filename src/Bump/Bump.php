<?php

/*
 * Copyright 2020 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Bump;

use Console\Prints;

/**
 * Helper command that bumps a package version.
 */
class Bump
{
    /**
     * Return code for failing to read "version" from files when reading them.
     *
     * @var int
     */
    public const ERROR_READ = 1;

    /**
     * Return code for failing to read "version" from files when writing them.
     *
     * @var int
     */
    public const ERROR_WRITE = 2;

    /**
     * Return code for successful bumping.
     *
     * @var int
     */
    public const OK = 0;

    /**
     * Regex string used to find the version information.
     *
     * @var string
     */
    public const REGEX = '/"version"\s*:\s*"(\d+)\.(\d+)\.(\d+)([^"]+)?"/';

    /**
     * List of files to version.
     *
     * @var string[]
     */
    public $files = [];

    /**
     * Bump constructor.
     *
     * @param string $files Files to bump
     */
    public function __construct(string $files)
    {
        $this->files = explode(',', $files);
    }

    /**
     * Bump files by a major version.
     *
     * @return int
     */
    public function major()
    {
        return $this->bump(static function (array $previous) {
            $version = [
                'major'     => $previous['major'] + 1,
                'minor'     => 0,
                'patch'     => 0,
                'candidate' => null,
            ];

            return $version + $previous;
        });
    }

    /**
     * Bump files by a minor version.
     *
     * @return int
     */
    public function minor()
    {
        return $this->bump(static function (array $previous) {
            $version = [
                'minor'     => $previous['minor'] + 1,
                'patch'     => 0,
                'candidate' => null,
            ];

            return $version + $previous;
        });
    }

    /**
     * Bump files by a patch version.
     *
     * @return int
     */
    public function patch()
    {
        return $this->bump(static function (array $previous) {
            $version = [
                'patch'     => $previous['patch'] + 1,
                'candidate' => null,
            ];

            // Preserve bumped keys over previous version
            return $version + $previous;
        });
    }

    /**
     * Display all file versions.
     *
     * @return int
     */
    public function version()
    {
        foreach ($this->files as $file) {
            if (false === ($version = $this->read($file))) {
                return self::ERROR_READ;
            }

            Prints::p($file.' version: '.$this->reduce($version));
        }

        return 0;
    }

    /**
     * Bumps a file to a specific version using a callback.
     *
     * @param callable $bump Function that returns the new version
     *
     * @return int
     */
    protected function bump(callable $bump)
    {
        // Stores the last file version
        $to = null;

        foreach ($this->files as $file) {
            if (false === ($version = $this->read($file))) {
                return self::ERROR_READ;
            }

            // Keeping a copy of the previous version to replace
            $from = $this->reduce($version);

            // Update patch version and remove candidacy if found
            $version = $bump($version);

            // Now get a copy of the new version
            $to = $this->reduce($version);

            // Write the file version
            if (false === $this->write($file, $from, $to)) {
                return self::ERROR_WRITE;
            }
        }

        // Prints helpful Git copy-paste messages
        $this->git($to);

        // OK
        return self::OK;
    }

    /**
     * Throws a common exception about no "version" found in file.
     *
     * @param string $file File that was read/written
     */
    protected function error(string $file)
    {
        Prints::p('ERROR Could not find "version" in '.$file);
    }

    /**
     * Prints helpful Git messages.
     *
     * @param string $to Version the files we bumped to
     */
    protected function git(string $to)
    {
        // Prompt users to review changes before pasting help messages
        Prints::p();
        Prints::p('Please review your Git changes before committing');
        Prints::p();

        // Helpful copy-paste commands for committing code to Git
        Prints::p('git add -- '.implode(' ', $this->files));

        // Generic commit message based off last file bumped
        Prints::p('git commit -m "Bumping to version '.$to.'"');

        // Adding a tag to it all
        Prints::p('git tag -a v'.$to.' -m "Releasing version '.$to.'"');

        // Final message sends everything
        Prints::p('git push && git push --tags');
    }

    /**
     * Reads a file and returns an array of integers to increment.
     *
     * @param string $file File to read
     *
     * @return array|false
     */
    protected function read(string $file)
    {
        // Make sure the file exists and is readable
        if (!is_readable($file)) {
            Prints::p('ERROR Could not read file '.$file);

            return false;
        }

        // Using an array of strings as buffer
        foreach (file($file) as $line) {
            // Match the JSON "version" attribute
            if (preg_match(self::REGEX, $line, $matches)) {
                // Extract version matches based on index
                [/* $matched */, $major, $minor, $patch] = $matches;

                // Candidate is optional and the list above will thrown an error
                return [
                    'major'     => (int) $major,
                    'minor'     => (int) $minor,
                    'patch'     => (int) $patch,
                    'candidate' => $matches[4] ?? null,
                ];
            }
        }

        // Print error message
        $this->error($file);

        // No lines matched "version" JSON
        return false;
    }

    /**
     * Returns a print friendly version.
     *
     * @param array $version Version information
     *
     * @return string
     */
    protected function reduce(array $version)
    {
        return $version['major'].'.'.
            $version['minor'].'.'.
            $version['patch'].($version['candidate'] ?? '');
    }

    /**
     * Writes the version to a file.
     *
     * @param string $file File to write
     * @param string $from Previous version
     * @param string $to   New version
     *
     * @return int|false {@see file_put_contents()}
     */
    protected function write(string $file, string $from, string $to)
    {
        // Using an array of strings as buffer
        $buffer = file($file);

        foreach ($buffer as $index => $line) {
            if (preg_match(self::REGEX, $line, $matches)) {
                Prints::p('Bumping '.$file.' version from '.$from.' to '.$to);

                // Update buffer index
                $buffer[$index] = str_replace($from, $to, $line);

                // Immediately attempt to write the file
                return file_put_contents($file, implode('', $buffer));
            }
        }

        // Print error message
        $this->error($file);

        // No lines matched "version" JSON
        return false;
    }
}
