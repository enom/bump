# Bump

Simple version bumping script.

[![Build](https://gitlab.com/enom/pharmon/badges/master/pipeline.svg)](https://gitlab.com/enom/pharmon/commits/master)
[![StyleCI](https://gitlab.styleci.io/repos/6522421/shield?branch=master)](https://gitlab.styleci.io/repos/6522421)

# Installation

Globally installing the script.

```sh
composer global require enom/bump
```

You will need to add Composer's bin directory to your PATH in `~/.bash_profile`
(or `~/.bashrc`) by adding the following line:

```sh
export PATH=~/.composer/vendor/bin:$PATH
```

# Usage

Display the current composer.json semver:

```sh
bump -v
# Displays (e.g.) "composer.json version: 1.2.3"
```

Bumps composer.json's semver.

```sh
bump -n
# Bumps composer.json's version from (e.g.) 1.2.3 to 1.3.0
```

You can also bump other JSON files that have a "version" attribute by specifying
them using `--files` and separating them with commas:

```sh
bump --patch --files composer.json,package.json
# Bumps both file versions from (e.g.) 1.2.3 to 1.2.4
```
